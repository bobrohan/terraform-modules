variable "instance_id" {
  type = string
  default = null
}

variable "vpc" { 
  default = true
}