resource "aws_eip" "instance" {
  instance = var.instance_id
  vpc      = var.vpc
}