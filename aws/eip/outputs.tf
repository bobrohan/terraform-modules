output "instance_eip" {
  value = aws_eip.instance.public_ip
}

output "eip_id" {
  value = aws_eip.instance.id
}

output "id" {
  value = aws_eip.instance.id
}