variable "name" {}
variable "key_name" {}
variable "aws_security_group_id" {}
variable "aws_subnet_id" {}
variable "private_ip" {}
variable "instance_type" {}
variable "ami" {
  type = string
  default = "ami-00152f16fe6a671c9"
}
variable "iam_instance_profile" {
  type = string
  default = ""
}