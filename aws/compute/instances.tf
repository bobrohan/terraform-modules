resource "aws_instance" "instance" {
  availability_zone = "eu-west-2a"
  # ubuntu-minimal/images-testing/hvm-ssd/ubuntu-bionic-daily-amd64-minimal-20190910
  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [var.aws_security_group_id]
  subnet_id = var.aws_subnet_id
  private_ip = var.private_ip
  key_name = var.key_name
  iam_instance_profile = var.iam_instance_profile
}