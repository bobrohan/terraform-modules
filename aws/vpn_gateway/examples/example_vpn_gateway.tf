module "vpn" {
  source = "../"
  
  vpc_id = module.vpc.id
  
  opt_tags = {
      Name = "eks aws vpn gateway"
  }
}