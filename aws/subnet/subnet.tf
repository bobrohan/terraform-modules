resource "aws_subnet" "instance" {
  availability_zone = var.availability_zone
  cidr_block = var.cidr_block
  vpc_id = var.vpc_id
  
  tags = var.opt_tags
}