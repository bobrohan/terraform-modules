module "eks-public-subnet-1" {

  source = "../"

  availability_zone = data.aws_availability_zones.available.names[0]
  cidr_block        = var.public_subnets[0]
  vpc_id            = module.eks_vpc.id

  tags = {
     "Name" = "eks-public-subnet-1",
     "kubernetes.io/cluster/${var.cluster-name}" = "shared",
    }
  
}