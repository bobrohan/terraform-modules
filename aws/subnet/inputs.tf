variable "availability_zone" {}
variable "cidr_block" {}
variable "vpc_id" {}

variable "opt_tags" {
  type = map
  default = {}
}