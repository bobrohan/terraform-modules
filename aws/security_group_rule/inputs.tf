variable "type" {}
variable "cidr_blocks" {}
variable "from_port" {}
variable "to_port" {}
variable "protocol" {}
variable "security_group_id" {}