resource "aws_eks_cluster" "instance" {
  name                      = var.name
  role_arn                  = var.role_arn 
  version                   = var.eks_version
  vpc_config {
    security_group_ids      = var.security_group_ids
    subnet_ids              = var.subnet_ids
    endpoint_private_access = var.endpoint_private_access
    endpoint_public_access  = var.endpoint_public_access
  }
}