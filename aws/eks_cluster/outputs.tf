output "endpoint" {
  value = aws_eks_cluster.instance.endpoint
}

output "certificate_authority_0_data" {
  value = aws_eks_cluster.instance.certificate_authority.0.data
}