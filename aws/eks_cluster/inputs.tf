variable "name" {}
variable "role_arn" {}
variable "eks_version" {}
variable "security_group_ids" {}
variable "subnet_ids" {}
variable "endpoint_private_access" {}
variable "endpoint_public_access" {}