module "eks-private-route-table-association-1" {

  source = "../"

  subnet_id      = aws_subnet.eks-private[0].id
  route_table_id = module.eks-private-route-table.id

}