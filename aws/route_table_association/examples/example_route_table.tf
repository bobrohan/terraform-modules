module "eks-private-route-table" {

  source = "../"

  vpc_id = module.eks_vpc.id

  opt_tags = {
        Name = "route table for private subnets"
  }
}

