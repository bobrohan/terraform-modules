## Enable internal route for NAT gw
module "eks-private-route" {

  source = "../"

  route_table_id = module.eks-private-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = module.nat_gw.id    
}