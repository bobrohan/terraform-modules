variable "name" {}
variable "vpc_id" {}

variable "opt_tags" {
  type = map
  default = {}
}