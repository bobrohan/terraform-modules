resource "aws_security_group" "instance" {
  name        = var.name
  vpc_id      = var.vpc_id
  
  tags = var.opt_tags
   
}