module "key_pair" {
  source = "../"
  name = "example-key-pair"
  pubkey = "ssh-rsa examplepublickey"
}