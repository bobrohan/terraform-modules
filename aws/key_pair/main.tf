resource "aws_key_pair" "instance" {
  key_name   = var.name
  public_key = var.pubkey
}