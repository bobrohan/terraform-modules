variable "route_table_id" {}
variable "destination_cidr_block" {
  default = null
}
variable "gateway_id" {
  default = null
}
variable "nat_gateway_id" {
  default = null
}