output "internet_gateway_id" {
  value = aws_internet_gateway.instance.id
}

output "id" {
  value = aws_internet_gateway.instance.id
}