output "cidr_block" {
  value = aws_vpc.instance.cidr_block
}

output "aws_vpc_id" {
  value = aws_vpc.instance.id
}

output "id" {
  value = aws_vpc.instance.id
}