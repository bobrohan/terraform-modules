resource "aws_vpc" "instance" {
  cidr_block = var.cidr_block
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support = true
  tags = var.opt_tags
  
}