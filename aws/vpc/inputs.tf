variable "enable_dns_hostnames" {}
variable "cidr_block" {}
variable "opt_tags" {
  type = map
  default = {}
}