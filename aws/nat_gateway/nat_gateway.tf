resource "aws_nat_gateway" "instance" {
  
  allocation_id = var.allocation_id
  subnet_id     = var.subnet_id

  tags = var.opt_tags
  
  depends_on = [var.instance_depends_on]
}