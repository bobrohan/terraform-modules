variable "allocation_id" {}
variable "subnet_id" {} 
variable "opt_tags" {
  default = {}
}

variable "instance_depends_on" {
  type    = any
  default = null
}