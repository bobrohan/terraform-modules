module "nat_gw" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/nat_gateway?ref=master"

  count = 1
  
  allocation_id = module.nat_eip.id
  subnet_id     = aws_subnet.eks-public.*.id[count.index] 
  depends_on = [aws_internet_gateway.eks-igw]

  opt_tags = {
    Name = "gw NAT"
  }
}