module "internet_gateway" {
  source = "../"

  vpc_id = module.vpc.id

  opt_tags = {
    Name = "eks-internet-gateway"
  }
}