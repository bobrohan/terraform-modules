resource "aws_route_table" "instance" {
  vpc_id = var.vpc_id
  
  tags = var.opt_tags
}